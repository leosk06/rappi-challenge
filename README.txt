Capas de la aplicación
----------------------

Las siguientes clases tienen varias responsabilidades y no pueden ser colocadas en una capa específica
	- clase app.App
	- clase app.MainActivity
	- todas herederas de Fragment (incluyendo mvvm.BaseFragment)

Capa de vista
	- paquete app.component
	- paquete app.ui
	- clase mvvm.MvvmView (pero no mvvm.BaseFragment que ya fue mencionada)

Capa de acceso a datos (repositorio)
	- paquete app.repo
	- Capa de persistencia: paquetes app.db y app.db.model
	- Capa de red: paquetes app.http y app.db.model

Capas de presentación:
	- clase mvvm.MvvmPresenter, DetailPresenter y CategoryPresenter
	- clases de modelo de vista
		- CategoryViewModel
		- DetailViewModel
	- En rigor no debería ser considerado parte de una "capa" de la app, sino una dependencia que podría venir desde una librería.

Capa de modelo del negocio
	- paquete app.model


Responsabilidad de cada clase
-----------------------------

app.App:
	- Inicializar el estado de la aplicación
	- Brindar acceso a recursos del sistema (como casi todo Context)
	- Brindar acceso a recursos de la aplicación (inyección de dependencias)
	- Inicializar herramientas de control de calidad en modo debug

app.MainActivity:
	- Visualiza todo lo visualizable en esta aplicación
	- Implementa navegación con transiciones visuales (basada en Fragments)
	- Brindar acceso a recursos del sistema (como casi todo Context)
	- muestra los errores ocurridos en toda la app
	- implementa la búsqueda offline
		- esta Activity debería tener un Presenter que implemente esto por separado. No lo hice así por una cuestión de tiempo.

mvvm.MvvmPresenter:
	- interactuar con las dependencias de la aplicación para actualizar su estado
	- comunicar el estado de la aplicación a las vistas (Mvvm)

mvvm.MvvmView:
	- realizar acciones de alto nivel requeridas por MvvmPresenter para reflejar en las vistas el estado de la aplicación

mvvm.BaseFragment:
	- crear y hacer disponible al usuario una parte de la UI
	- realizar acciones de alto nivel requeridas por MvvmPresenter para reflejar en su UI el estado de la aplicación

app.model.ScreenEntertainment:
	- representa una película o una serie de TV ("entretenimientos para pantalla")

app.model.Categories:
	- representa los tres criterios para mostrar películas o series: popular, más votados y próximos a estrenar

app.model.ItemTypes
	- representa las dos posibles clases de ScreenEntertainment.

app.model.Result
	- representa de manera sencilla el resultado de una operación cualquiera, que puede ser exitosa o no

app.model.http.TsmbRestApi
	- declaración de los endpoints para el servicio REST que consume la aplicación

app.model.http.model.*
	- modelos para la capa de red, representan entidades devueltas por el servicio REST

app.model.db.DbClient
	- realiza operaciones de alto nivel de consulta y actualización de la base de datos

app.model.db.model.*
	- modelos para la capa de acceso a datos, representan entidades tal y como se guardan en la DB

app.repo.EntertainmentRepository
	- se sirve de las capas de red y acceso a datos para implementar operaciones de consulta de manera "online first"

app.ui.ScreenEntertainmentList
	- muestra un conjunto de películas en una lista horizontal

app.ui.search.ScreenEntertainmentCursor
	- representa un "puntero" dentro una lista de películas o series de TV, el cual permite consultar cada elemento de la lista

app.ui.search.SuggestionsAdapter
	- a partir de instancias del modelo de películas y series de TV, crea vistas que las representan

app.di.AppComponent
	- declaración de un objeto cuya responsabilidad será inyectar dependencias en otros objetos de diversas clases

app.di.AppModule
	- crea los objetos que inyectará AppComponent

app.di.CategoryPresenters
	- crea instancias de CategoryPresenter y las provee a quien las solicite (se usa para inyectar los Presenters como dependencias)

app.component.main.MainFragment
	- crea la interfaz de la pantalla principal de la aplicación

app.component.detail.DetailFragment
	- crea la interfaz para la vista de detalle

app.component.detail.DetailViewModel
	- contiene el estado de la vista de detalle que debe mostrar DetailFragment

app.component.detail.DetailPresenter
	- interactúa con los componentes de la aplicación para determinar la información que se mostrará en DetailFragment
	- comunica a DetailFragment dicha información encapsulada en un DetailViewModel

app.component.category.CategoryFragment
	- crea la interfaz para la vista de una categoría (lista de películas o series de TV según uno de los criterios)

app.component.category.CategorylViewModel
	- contiene el estado de la vista de categorías que debe mostrar CategoryFragment

app.component.category.CategoryPresenter
	- interactúa con los componentes de la aplicación para determinar la información que se mostrará en CategoryFragment
	- comunica a CategoryFragment dicha información encapsulada en un CategoryViewModel

app.Navigator
	- describe las operaciones de navegación de la aplicación

util.StrictModeUtil
	- configura el StrictMode de Android de la manera que estimé más conveniente

util.SystemPropertyUtil
	- lee propiedades del sistema (lo usa StrictModeUtil para decidir si prohíbe el tráfico de red no encriptado)




Preguntas
---------
1. En qué consiste el principio de responsabilidad única? Cuál es su propósito?

El principio de responsabilidad única establece que cada clase o cada función que programemos debe limitarse a hacer una sola cosa.

Con esto se logra que los bloques de nuestro software sean:
- más reutilizables
- más fáciles de comprender en su totalidad
- más fáciles de reemplazar por mocks, y por lo tanto más testeables

Cabe destacar que la inyección de dependencias es consecuencia de este principio, ya que si una entidad de nuestro proyecto necesita una dependencia para realizar su responsabilidad, entonces crear dicha dependencia le agregaría una segunda responsabilidad, lo cual contraviene el principio.


2. Qué características tiene, según su opinión, un “buen” código o código limpio?

En mi opinión, el código limpio tiene las siguientes características:
- está separado en capas
	- y cada capa solamente accede a sus capas inferiores adyacentes
- sigue el principio de responsabilidad única por medio de la implementación de patrones de diseño y arquitectura
	- y en consecuencia utiliza inyección de dependencias
- ninguna de sus entidades es muy extensa, y cuando comienza a serlo, se separa en más entidades
- no hay código repetido
- todas los literales están asignados a constantes (ie. no hay "números mágicos")
- los nombres de todas sus entidades:
	- son descriptivos
	- siguen convenciones
- incluye código de prueba con buena cobertura
	- y este código también es código limpio
- está debidamente documentado
	- la documentación está actualizada
	- sólo se documentan cosas que no surjan claramente del código
- presenta la complejidad mínima necesaria para cumplir los requerimientos
- las funciones son, en lo posible, puras
- no se puede acceder al estado interno de los objetos

