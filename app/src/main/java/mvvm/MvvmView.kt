package mvvm

/**
 * Created by leoacevedo on 27/7/17.
 */
interface MvvmView<T> {
    fun onNewViewModel(viewModel: T)
}