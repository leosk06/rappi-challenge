package mvvm

import android.support.annotation.CallSuper
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject

/**
 * Created by leoacevedo on 27/7/17.
 */
abstract class MvvmPresenter<T: Any> {
    protected lateinit var viewModel: T

    private val publisher = BehaviorSubject.create<T>().toSerialized()
    private val disposables = CompositeDisposable()

    internal val observeViewStates: Flowable<T> = Flowable.create(
            { subscriber: FlowableEmitter<T> ->
                val disposable = publisher.subscribe(
                        subscriber::onNext,
                        subscriber::onError
                )
                subscriber.setDisposable(disposable)
            },
            BackpressureStrategy.LATEST
    )


    open fun dismissedErrorMessage() {

    }

    @CallSuper
    open fun onShowing() {

    }

    @CallSuper
    open fun onHiding() {
        disposables.clear()
    }

    fun addDisposable(vararg d: Disposable) {
        d.forEach { disposables.add(it) }
    }

    fun emitViewModel(viewModel: T) {
        this.viewModel = viewModel
        publisher.onNext(viewModel)
    }
}