package mvvm

import android.animation.LayoutTransition
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import app.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


/**
 * Created by leoacevedo on 2017/8/1.
 */
abstract class BaseFragment<V : Any, P : MvvmPresenter<V>> : Fragment(), MvvmView<V> {
    protected lateinit var presenter: P

    private val disposables = CompositeDisposable()


    abstract override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (view as ViewGroup).layoutTransition?.enableTransitionType(LayoutTransition.CHANGING)
    }

    override fun onStart() {
        super.onStart()
        addDisposable(
                presenter.observeViewStates
                        .distinctUntilChanged()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::onNewViewModel)
        )
        presenter.onShowing()
    }

    override fun onStop() {
        super.onStop()
        presenter.onHiding()
        clearDisposables()
    }

    fun addDisposable(d: Disposable) {
        disposables.add(d)
    }

    fun clearDisposables() {
        disposables.clear()
    }

    protected abstract fun inject()

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        val parent = parentFragment

        // Apply the workaround only if this is a child fragment, and the parent
        // is being removed.
        if (!enter && parent != null && parent.isRemoving) {
            // This is a workaround for the bug where child fragments disappear when
            // the parent is removed (as all children are first removed from the parent)
            // See https://code.google.com/p/android/issues/detail?id=55228
            val doNothingAnim = AlphaAnimation(1f, 1f)
            doNothingAnim.duration = resources.getInteger(R.integer.transition_animation_duration).toLong()
            return doNothingAnim
        } else {
            return super.onCreateAnimation(transit, enter, nextAnim)
        }
    }
}