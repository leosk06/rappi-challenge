package util

import android.os.Build
import android.os.StrictMode

/**
 * Created by leo on 2018/3/18.
 *
 * See https://github.com/square/okhttp/issues/3537
 */
class StrictModeUtil {
    companion object {
        private val CLEARTEXT_PROPERTY = "persist.sys.strictmode.clear"

        fun vmPolicyDetectingAllButUntaggedSockets(): StrictMode.VmPolicy {
            val builder = StrictMode.VmPolicy.Builder()

            with(builder) {
                detectLeakedSqlLiteObjects()
                detectActivityLeaks()
                // Ignoring the following because OkHttp will call GzipSource and crash without my control
                // detectLeakedClosableObjects()
                detectLeakedRegistrationObjects()

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    detectFileUriExposure()
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (SystemPropertyUtil.getBoolean(CLEARTEXT_PROPERTY, false)) {
                        detectCleartextNetwork()
                    }
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    detectContentUriWithoutPermission()
                }

                penaltyLog()
                penaltyDeath()
            }

            return builder.build()
        }
    }
}