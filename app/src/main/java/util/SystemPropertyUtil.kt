package util

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

/**
 * Created by leo on 2018/3/18.
 */
class SystemPropertyUtil {
    companion object {
        private fun getProperty(propertyName: String): String {
            return try {
                val process = Runtime.getRuntime().exec("getprop $propertyName")
                val propertyReader = BufferedReader(InputStreamReader(process.inputStream))
                val propertyValue = propertyReader.readLine()
                process.destroy()
                propertyValue
            } catch (e: IOException) {
                ""
            }
        }

        fun getBoolean(propertyName: String, default: Boolean): Boolean {
            val propertyValue = getProperty(propertyName)

            return if (propertyValue.isEmpty()) {
                default
            } else {
                propertyValue.toBoolean()
            }
        }
    }
}