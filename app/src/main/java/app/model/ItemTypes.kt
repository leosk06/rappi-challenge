package app.model

import android.support.annotation.IntDef

class ItemTypes {
    companion object {
        const val MOVIE = 0
        const val TV_SHOW = 1

        @IntDef(MOVIE, TV_SHOW)
        annotation class ItemType
    }
}