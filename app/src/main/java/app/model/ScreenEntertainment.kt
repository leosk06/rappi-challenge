package app.model

data class ScreenEntertainment(
    val id: String = "",
    val isMovie: Boolean = false,
    val voteCount: Int = -1,
    val title: String = "",
    val voteAverage: Float = -1f,
    val posterPath: String = "",
    val backdropPath: String = "",
    val overview: String = "",
    val hasVideo: Boolean = false,
    val releaseDate: String = ""
) {
    companion object {
        val EMPTY = ScreenEntertainment()
    }
}