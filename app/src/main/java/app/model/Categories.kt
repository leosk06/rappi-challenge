package app.model

import android.support.annotation.IntDef

class Categories {
    companion object {
        const val POPULAR = 0
        const val TOP_RATED = 1
        const val UPCOMING = 2

        @IntDef(POPULAR, TOP_RATED, UPCOMING)
        annotation class Category
    }
}