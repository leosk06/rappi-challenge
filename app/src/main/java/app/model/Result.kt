package app.model

data class Result<T>(
        val payload: T?,
        val error: Throwable?
)