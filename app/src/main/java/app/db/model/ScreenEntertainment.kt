package app.db.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ScreenEntertainment() : RealmObject() {
    companion object {
        val PREFIX_MOVIE = "m"
        val PREFIX_SERIES = "s"
    }

    constructor(classPrefix: String, inClassId: String) : this() {
        id = "$classPrefix$inClassId"
    }

    // Simulate a compound key by concatenaing class and ID
    @PrimaryKey
    var id: String = ""

    var title: String = ""
    var voteCount: Int = -1
    var voteAverage: Double = -1.0
    var posterPath: String = ""
    var backdropPath: String = ""
    var overview: String = ""
    var releaseDate: String = ""
    var hasVideo: Boolean = false
}