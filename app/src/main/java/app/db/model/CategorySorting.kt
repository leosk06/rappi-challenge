package app.db.model

import io.realm.RealmObject


open class CategorySorting() : RealmObject() {
    var id: String = ""
    var category: Int = -1
    var order: Int = -1

    constructor(id: String, category: Int, order: Int): this() {
        this.id = id
        this.category = category
        this.order = order
    }
}