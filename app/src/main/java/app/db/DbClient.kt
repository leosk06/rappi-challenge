package app.db

import android.os.Handler
import app.db.model.CategorySorting
import app.db.model.ScreenEntertainment
import app.model.Categories.Companion.Category
import app.model.ItemTypes
import app.model.ItemTypes.Companion.ItemType
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import io.realm.*
import java.util.*

class DbClient(val realm: Realm) {
    private val handler = Handler()

    fun getMovieByCriteria(@Category category: Int): Observable<List<ScreenEntertainment>> {
        return getItemsByCriteria(ItemTypes.MOVIE, category)
    }

    fun getTvSeriesByCriteria(@Category category: Int): Observable<List<ScreenEntertainment>> {
        return getItemsByCriteria(ItemTypes.TV_SHOW, category)
    }

    private fun getItemsByCriteria(@ItemType type: Int, @Category category: Int): Observable<List<ScreenEntertainment>> {
        val prefix = if (type == ItemTypes.MOVIE) ScreenEntertainment.PREFIX_MOVIE else ScreenEntertainment.PREFIX_SERIES
        val realmChanges = realm.where(CategorySorting::class.java)
                .equalTo("category", category)
                .beginsWith("id", prefix)
                .findAll()

        val subject = BehaviorSubject.create<List<ScreenEntertainment>>().toSerialized()
        val changeListener = RealmChangeListener<RealmResults<CategorySorting>> {
            val snapshot = realmChanges.sort("order").createSnapshot()

            // I presume there's always results
            if (snapshot.isNotEmpty()) {
                val sortingList: ArrayList<ScreenEntertainment> =
                        snapshot.map { sorting ->
                            realm.where(ScreenEntertainment::class.java)
                                    .equalTo("id", sorting.id)
                                    .findFirst()
                        }.toCollection(ArrayList())
                subject.onNext(sortingList)
            }
        }

        // Have something ready to publish
        changeListener.onChange(realmChanges)

        return Observable.create { subscriber: ObservableEmitter<List<ScreenEntertainment>> ->
            val disposable = subject.subscribe(
                    {
                        subscriber.onNext(it)
                    },
                    { subscriber.onError(it) }
            )

            realmChanges.addChangeListener(changeListener)

            subscriber.setDisposable(object : Disposable {
                override fun dispose() {
                    disposable.dispose()
                    realmChanges.removeChangeListener(changeListener)
                }

                override fun isDisposed() = disposable.isDisposed
            })
        }
    }

    @Suppress("NAME_SHADOWING")
    private fun <T : RealmModel> observeQuery(query: RealmQuery<T>): Observable<List<T>> {
        val subject: BehaviorSubject<List<T>> = BehaviorSubject.create()
        val results = query.findAllAsync()
        results.addChangeListener { results ->
            publishSnapshot(results, subject)
        }
        publishSnapshot(results, subject)
        return subject
    }

    private fun <T : RealmModel> publishSnapshot(results: RealmResults<T>, observer: Observer<List<T>>) {
        val snapshot = results.createSnapshot()
        val list = ArrayList<T>()
        list.addAll(snapshot)
        observer.onNext(list)
    }

    fun updateScreenEntertainment(item: ScreenEntertainment) {
        handler.post {
            realm.let {
                it.beginTransaction()
                it.copyToRealmOrUpdate(item)
                it.commitTransaction()
            }
        }
    }

    fun updateScreenEntertainment(items: List<ScreenEntertainment>) {
        handler.post {
            realm.let {
                it.beginTransaction()
                it.copyToRealmOrUpdate(items)
                it.commitTransaction()
            }
        }
    }

    fun updateOrderings(sortings: List<CategorySorting>) {
        if (sortings.isEmpty()) {
            return
        }

        val category = sortings[0].category
        val ids = sortings
                .map { it.id }
                .toTypedArray()

        handler.post {
            realm.let { realm ->
                realm.beginTransaction()
                realm.where(CategorySorting::class.java)
                        .equalTo("category", category)
                        .`in`("id", ids)
                        .findAll()
                        .deleteAllFromRealm()

                realm.copyToRealmOrUpdate(sortings)
                realm.commitTransaction()
            }
        }
    }

    fun getItem(itemId: String): Observable<ScreenEntertainment> {
        val realmChanges = realm.where(ScreenEntertainment::class.java)
                .equalTo("id", itemId)
                .findAll()

        val subject = BehaviorSubject.create<ScreenEntertainment>().toSerialized()
        val changeListener = RealmChangeListener<RealmResults<ScreenEntertainment>> {
            val snapshot = realmChanges.createSnapshot()

            if (snapshot.isNotEmpty()) {
                val item = snapshot.first()
                subject.onNext(item)
            }
        }

        // Have something ready to publish
        changeListener.onChange(realmChanges)

        return Observable.create { subscriber: ObservableEmitter<ScreenEntertainment> ->
            val disposable = subject.subscribe(
                    {
                        subscriber.onNext(it)
                    },
                    { subscriber.onError(it) }
            )

            realmChanges.addChangeListener(changeListener)

            subscriber.setDisposable(object : Disposable {
                override fun dispose() {
                    disposable.dispose()
                    realmChanges.removeChangeListener(changeListener)
                }

                override fun isDisposed() = disposable.isDisposed
            })
        }
    }

    fun search(query: String): Observable<List<ScreenEntertainment>> {
        val realmChanges = realm.where(ScreenEntertainment::class.java)
                .like("title", "*$query*", Case.INSENSITIVE)
                .findAll()

        val subject = BehaviorSubject.create<List<ScreenEntertainment>>().toSerialized()
        val changeListener = RealmChangeListener<RealmResults<ScreenEntertainment>> {
            val snapshot = realmChanges.createSnapshot()

            // I presume there's always results
            if (snapshot.isNotEmpty()) {
                val sortingList: ArrayList<ScreenEntertainment> =
                        snapshot.map { sorting ->
                            realm.where(ScreenEntertainment::class.java)
                                    .equalTo("id", sorting.id)
                                    .findFirst()
                        }.toCollection(ArrayList())
                subject.onNext(sortingList)
            }
        }

        // Have something ready to publish
        changeListener.onChange(realmChanges)

        return Observable.create { subscriber: ObservableEmitter<List<ScreenEntertainment>> ->
            val disposable = subject.subscribe(
                    {
                        subscriber.onNext(it)
                    },
                    { subscriber.onError(it) }
            )

            realmChanges.addChangeListener(changeListener)

            subscriber.setDisposable(object : Disposable {
                override fun dispose() {
                    disposable.dispose()
                    realmChanges.removeChangeListener(changeListener)
                }

                override fun isDisposed() = disposable.isDisposed
            })
        }
    }
}
