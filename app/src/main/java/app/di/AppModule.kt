package app.di

import app.App
import app.BuildConfig
import app.db.DbClient
import app.http.TsmbRestApi
import app.repo.EntertainmentRepository
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.realm.Realm
import io.realm.RealmConfiguration
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by leo on 2018/3/18.
 */

@Module
class AppModule(
        val baseUrl: String,
        val apiKey: String,
        val timeoutSecs: Long
) {
    @Provides
    @Singleton
    fun provideDb(app: App): Realm {
        Realm.init(app)
        Realm.setDefaultConfiguration(RealmConfiguration.Builder()
                .compactOnLaunch()
                .deleteRealmIfMigrationNeeded()
                .build()
        )
        return Realm.getDefaultInstance()
    }

    @Provides
    @Singleton
    fun provideDbClient(realm: Realm): DbClient {
        return DbClient(realm)
    }

    @Provides
    @Singleton
    fun provideGson() = Gson()

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
                .connectTimeout(timeoutSecs, TimeUnit.SECONDS)
                .writeTimeout(timeoutSecs, TimeUnit.SECONDS)
                .readTimeout(timeoutSecs, TimeUnit.SECONDS)
                .addInterceptor { chain ->
                    val originalRequest = chain.request()
                    val newRequestBuilder = originalRequest.newBuilder()
                            .url(originalRequest.url().newBuilder()
                                    .addQueryParameter("api_key", apiKey)
                                    .build()
                            )

                    chain.proceed(newRequestBuilder.build())
                }

        if (BuildConfig.DEBUG) {
            clientBuilder.addInterceptor(StethoInterceptor())
        }
        return clientBuilder.build()
    }

    @Provides
    @Singleton
    fun provideTsmbApiClient(okHttpClient: OkHttpClient, gson: Gson): TsmbRestApi {
        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        return retrofit.create(TsmbRestApi::class.java)
    }

    @Provides
    @Singleton
    fun provideEntertainmentRepository(restClient: TsmbRestApi, dbClient: DbClient): EntertainmentRepository {
        return EntertainmentRepository(restClient, dbClient)
    }
}
