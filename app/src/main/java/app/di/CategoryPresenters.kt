package app.di

import app.component.category.CategoryPresenter
import app.model.Categories.Companion.Category
import app.model.ItemTypes.Companion.ItemType

class CategoryPresenters {
    companion object {
        private val presenters = HashMap<Pair<Int, Int>, CategoryPresenter>()

        fun of(@ItemType itemType: Int, @Category category: Int): CategoryPresenter {
            synchronized(this) {
                val key = Pair(itemType, category)
                if (presenters[key] == null) {
                    presenters[key] = CategoryPresenter(itemType, category)
                }
                return presenters[key]!!
            }
        }
    }
}