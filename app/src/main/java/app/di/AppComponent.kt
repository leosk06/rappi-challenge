package app.di

import app.App
import app.MainActivity
import app.component.category.CategoryFragment
import app.component.category.CategoryPresenter
import app.component.detail.DetailFragment
import app.component.detail.DetailPresenter
import app.component.main.MainFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/**
 * Created by leo on 2018/3/18.
 */

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun app(): App

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(app: App): Builder

        fun appModule(appModule: AppModule): Builder
        fun build(): AppComponent
    }

    fun inject(target: MainActivity)
    fun inject(target: CategoryFragment)
    fun inject(target: CategoryPresenter)
    fun inject(target: MainFragment)
    fun inject(target: DetailFragment)
    fun inject(target: DetailPresenter)
}
