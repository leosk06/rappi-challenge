package app

import app.model.ScreenEntertainment

interface Navigator {
    fun gotoDetail(item: ScreenEntertainment)
}
