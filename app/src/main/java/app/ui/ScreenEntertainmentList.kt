package app.ui

import android.content.Context
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import app.Navigator
import app.R
import app.model.ScreenEntertainment
import com.squareup.picasso.Picasso

class ScreenEntertainmentList(context: Context, attrs: AttributeSet) : RecyclerView(context, attrs) {
    init {
        minimumHeight = resources.getDimensionPixelOffset(R.dimen.item_list_height)
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        clipToPadding = true
        overScrollMode = View.OVER_SCROLL_NEVER

        val dividerDecoration = DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL)
        dividerDecoration.setDrawable(ResourcesCompat.getDrawable(resources, R.drawable.item_list_divider, null)!!)
        addItemDecoration(dividerDecoration)
    }
}

class ScreenEntertainmentAdapter(
        val context: Context,
        val navigator: Navigator
) : RecyclerView.Adapter<ScreenEntertainmentVH>() {
    private var data: List<ScreenEntertainment> = ArrayList()
    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScreenEntertainmentVH {
        val itemView = inflater.inflate(R.layout.list_item, null, false)
        return ScreenEntertainmentVH(itemView, navigator)
    }

    override fun getItemCount(): Int {
        return data.count()
    }

    override fun onBindViewHolder(holder: ScreenEntertainmentVH, position: Int) {
        val model = data[position]
        holder.model = model
        startNewPicassoRequest(holder, "http://image.tmdb.org/t/p/w154/" + model.posterPath)
    }

    private fun startNewPicassoRequest(holder: ScreenEntertainmentVH, url: String) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.clock)
                .resize(
                        context.resources.getDimensionPixelOffset(R.dimen.item_list_width),
                        context.resources.getDimensionPixelOffset(R.dimen.item_list_height)
                )
                .centerCrop()
                .into(holder.image)
    }

    fun setData(result: List<ScreenEntertainment>) {
        data = result
        notifyDataSetChanged()
    }
}

class ScreenEntertainmentVH(itemView: View, navigator: Navigator) : RecyclerView.ViewHolder(itemView) {
    val image: ImageView = itemView.findViewById(R.id.image)
    var model: ScreenEntertainment? = null

    init {
        image.setOnClickListener { _ ->
            model?.let {
                navigator.gotoDetail(it)
            }
        }
    }
}