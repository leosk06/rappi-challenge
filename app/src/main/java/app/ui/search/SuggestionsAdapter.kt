package app.ui.search

import android.content.Context
import android.database.Cursor
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CursorAdapter
import android.widget.TextView
import app.R
import app.model.ScreenEntertainment
import app.repo.ModelItem

class SuggestionsAdapter(context: Context, items: List<ModelItem>) : CursorAdapter(
        context,
        ScreenEntertainmentCursor(items),
        false
) {
    var itemClickListener: OnSuggestionSelectedListener? = null
    private val inflater = LayoutInflater.from(context)


    override fun newView(context: Context, cursor: Cursor, parent: ViewGroup?): View {
        val view = inflater.inflate(R.layout.search_result_item, parent, false)
        view.setTag(R.id.search_title, view.findViewById(R.id.title))
        view.setTag(R.id.search_year, view.findViewById(R.id.year))
        view.setTag(R.id.search_type, view.findViewById(R.id.type))
        view.setOnClickListener {
            itemClickListener?.let { listener ->
                listener.onSuggestionSelected(
                        view.getTag(R.id.search_item) as ScreenEntertainment
                )
            }
        }
        return view
    }

    override fun bindView(view: View, context: Context, cursor: Cursor) {
        cursor as ScreenEntertainmentCursor
        val item = cursor.currentItem
        view.setTag(R.id.search_item, item)
        (view.getTag(R.id.search_title) as TextView).text = item?.title
        (view.getTag(R.id.search_year) as TextView).text = item?.releaseDate
        (view.getTag(R.id.search_type) as TextView).setText(
                if (item?.isMovie == true) R.string.type_movie else R.string.type_series
        )
    }
}

interface OnSuggestionSelectedListener {
    fun onSuggestionSelected(selected: ScreenEntertainment)
}
