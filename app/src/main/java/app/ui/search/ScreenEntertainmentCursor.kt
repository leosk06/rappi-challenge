package app.ui.search

import android.database.AbstractCursor
import app.model.ScreenEntertainment
import app.repo.ModelItem

class ScreenEntertainmentCursor(val items: List<ModelItem>) : AbstractCursor() {
    var currentItem: ScreenEntertainment? = null

    override fun getLong(column: Int): Long {
        if (column == 0) {
            return position.toLong()
        }
        crash()
    }

    override fun getCount() = items.size

    override fun getColumnNames() = arrayOf("_id", "id")

    override fun getShort(column: Int): Short {
        crash()
    }

    override fun getFloat(column: Int): Float {
        crash()
    }

    override fun getDouble(column: Int): Double {
        crash()
    }

    override fun isNull(column: Int): Boolean {
        crash()
    }

    override fun getInt(column: Int): Int {
        crash()
    }

    override fun getString(column: Int): String {
        crash()
    }

    private fun crash(): Nothing {
        throw UnsupportedOperationException()
    }

    override fun onMove(oldPosition: Int, newPosition: Int): Boolean {
        currentItem = items[newPosition]
        return super.onMove(oldPosition, newPosition)
    }
}