package app.repo

import android.util.Log
import app.db.DbClient
import app.db.model.CategorySorting
import app.db.model.ScreenEntertainment
import app.http.TsmbRestApi
import app.http.model.Movie
import app.model.Categories
import app.model.Categories.Companion.Category
import app.model.ItemTypes
import app.model.ItemTypes.Companion.ItemType
import app.model.Result
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

internal typealias ModelItem = app.model.ScreenEntertainment
internal typealias RestItem = app.http.model.ScreenEntertainment
internal typealias RestMovie = app.http.model.Movie
internal typealias DbItem = app.db.model.ScreenEntertainment

class EntertainmentRepository(
        private val restClient: TsmbRestApi,
        private val dbClient: DbClient
) {
    private fun toDbObject(restItem: RestItem): DbItem {
        val result = DbItem(
                classPrefix = when (restItem) {
                    is RestMovie -> ScreenEntertainment.PREFIX_MOVIE
                    else -> ScreenEntertainment.PREFIX_SERIES
                },
                inClassId = restItem.id!!
        )

        with(result) {
            voteCount = restItem.voteCount
            title = restItem.title.orEmpty()
            voteAverage = restItem.voteAverage
            posterPath = restItem.posterPath.orEmpty()
            backdropPath = restItem.backdropPath.orEmpty()
            overview = restItem.overview.orEmpty()
            hasVideo = restItem is Movie && restItem.hasVideo
            releaseDate = restItem.releaseDate.orEmpty()
        }

        return result
    }

    private fun toModelObject(restItem: RestItem) = ModelItem(
            id = restItem.id.orEmpty(),
            isMovie = restItem is RestMovie,
            voteCount = restItem.voteCount,
            title = restItem.title.orEmpty(),
            voteAverage = restItem.voteAverage.toFloat(),
            posterPath = restItem.posterPath.orEmpty(),
            backdropPath = restItem.backdropPath.orEmpty(),
            overview = restItem.overview.orEmpty(),
            hasVideo = restItem.hasVideo,
            releaseDate = restItem.releaseDate.orEmpty()
    )

    private fun toModelObject(dbItem: DbItem): ModelItem = ModelItem(
            id = dbItem.id,
            isMovie = dbItem.id.startsWith(ScreenEntertainment.PREFIX_MOVIE),
            voteCount = dbItem.voteCount,
            title = dbItem.title,
            voteAverage = dbItem.voteAverage.toFloat(),
            posterPath = dbItem.posterPath,
            backdropPath = dbItem.backdropPath,
            overview = dbItem.overview,
            hasVideo = dbItem.hasVideo,
            releaseDate = dbItem.releaseDate
    )

    fun getMovieByCriteria(@Category category: Int, page: Int): Observable<Result<List<ModelItem>>> {
        return getItemsByCriteria(ItemTypes.MOVIE, category, page)
    }

    fun getTvSeriesByCriteria(@Category category: Int, page: Int): Observable<Result<List<ModelItem>>> {
        return getItemsByCriteria(ItemTypes.TV_SHOW, category, page)
    }

    private fun apiCategory(@Category category: Int) =
            when (category) {
                Categories.POPULAR -> TsmbRestApi.CRITERIA_POPULAR
                Categories.TOP_RATED -> TsmbRestApi.CRITERIA_TOP_RATED
                else -> TsmbRestApi.CRITERIA_UPCOMING
            }

    private fun getItemsByCriteria(@ItemType type: Int, @Category category: Int, page: Int): Observable<Result<List<ModelItem>>> {
        val apiCategory = apiCategory(category)

        val restStream = if (type == ItemTypes.MOVIE)
            restClient.getMovieByCriteria(apiCategory, page)
        else
            restClient.getTvSeriesByCriteria(apiCategory, page)

        restStream
                .subscribeOn(Schedulers.io())
                .map { it.results.toList() }
                .subscribe(
                        { results ->
                            val dbItems = results.map { toDbObject(it) }
                            dbClient.updateScreenEntertainment(dbItems)

                            val orderings = dbItems.map { dbItem ->
                                CategorySorting(
                                        dbItem.id,
                                        category,
                                        dbItems.indexOf(dbItem)
                                )
                            }
                            dbClient.updateOrderings(orderings)
                        },
                        { error ->
                            Log.e("ERROR", "Error using REST service", error)
                        }
                )

        val dbStream = if (type == ItemTypes.MOVIE)
            dbClient.getMovieByCriteria(category)
        else
            dbClient.getTvSeriesByCriteria(category)

        return dbStream
                .observeOn(AndroidSchedulers.mainThread())
                .map { items ->
                    Result(items.map { toModelObject(it) }, null)
                }
                .onErrorResumeNext { error: Throwable ->
                    Observable.just(
                            Result(Collections.emptyList(), error)
                    )
                }
    }


    fun getItemDetail(itemId: String): Observable<Result<ModelItem>> {
        restClient.getMovieDetail(itemId)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        { restItem ->
                            val dbItem = toDbObject(restItem)
                            dbClient.updateScreenEntertainment(dbItem)
                        },
                        { error ->
                            Log.e("ERROR", "Error using REST service", error)
                        }

                )

        return dbClient.getItem(itemId)
                .observeOn(AndroidSchedulers.mainThread())
                .map { item ->
                    Result(toModelObject(item), null)
                }
                .onErrorResumeNext { error: Throwable ->
                    Observable.just(
                            Result(ModelItem.EMPTY, error)
                    )
                }
    }

    fun search(query: String): Observable<List<ModelItem>> {
        return dbClient.search(query)
                .observeOn(AndroidSchedulers.mainThread())
                .map { items ->
                    items.map { toModelObject(it) }
                }
                .onErrorResumeNext { _: Throwable -> Observable.just(Collections.emptyList()) }
    }
}