package app

import android.os.StrictMode
import android.support.multidex.MultiDexApplication
import app.di.AppComponent
import app.di.AppModule
import app.di.DaggerAppComponent
import util.StrictModeUtil
import com.crashlytics.android.Crashlytics
import com.facebook.stetho.Stetho
import com.squareup.leakcanary.LeakCanary
import com.uphyca.stetho_realm.RealmInspectorModulesProvider
import io.fabric.sdk.android.Fabric

/**
 * Created by leo on 2018/3/18.
 */

class App : MultiDexApplication() {
    companion object {
        lateinit var instance: App private set
        lateinit var component: AppComponent private set
    }

    override fun onCreate() {
        super.onCreate()

        initializeCrashlytics()

        instance = this
        component = DaggerAppComponent.builder()
                .app(this)
                .appModule(AppModule(
                        timeoutSecs = 60000,
                        baseUrl = "https://api.themoviedb.org/3/",
                        apiKey = getString(R.string.api_key)
                ))
                .build()

        enableStetho()
        startLeakCanary()

        if (BuildConfig.DEBUG) {
            pedanticStrictMode()
        }
    }

    private fun initializeCrashlytics() {
        Fabric.with(this, Crashlytics())
    }

    private fun enableStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initialize(
                    Stetho.newInitializerBuilder(this)
                            .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                            .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                            .build()
            )
        }
    }

    private fun startLeakCanary() {
        if (BuildConfig.DEBUG) {
            if (!LeakCanary.isInAnalyzerProcess(this)) {
                LeakCanary.install(this)
            }
        }
    }

    private fun pedanticStrictMode() {
        StrictMode.setVmPolicy(
                StrictModeUtil.vmPolicyDetectingAllButUntaggedSockets()
        )
        StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder()
                        .detectAll()
                        .penaltyLog()
                        .penaltyDeath()
                        .build()
        )
    }
}
