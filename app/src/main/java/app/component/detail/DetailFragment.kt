package app.component.detail

import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import app.App
import app.R
import app.model.ItemTypes
import app.model.ScreenEntertainment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_detail.*
import mvvm.BaseFragment


class DetailFragment : BaseFragment<DetailViewModel, DetailPresenter>() {
    companion object {
        private const val ARG_ITEM_TYPE = "itt"
        private const val ARG_ID = "id"
        private const val ARG_TITLE = "ttl"

        fun newInstance(item: ScreenEntertainment): DetailFragment {
            val instance = DetailFragment()
            val args = Bundle()
            args.putString(ARG_ID, item.id!!)
            args.putString(ARG_TITLE, item.title!!)
            args.putInt(ARG_ITEM_TYPE, if (item.isMovie) ItemTypes.MOVIE else ItemTypes.TV_SHOW)
            instance.arguments = args
            return instance
        }
    }

    private var itemType = -1
    private var itemId = ""
    private var itemTitle = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let { args ->
            itemType = args.getInt(ARG_ITEM_TYPE)
            itemId = args.getString(ARG_ID)
            itemTitle = args.getString(ARG_TITLE)
        }
        presenter = DetailPresenter(itemType, itemId = itemId, title = itemTitle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tint(rating, R.color.yellow)
        val activity = activity as AppCompatActivity
        activity.setSupportActionBar(toolbar)
        activity.supportActionBar!!.setDisplayShowHomeEnabled(true)
        activity.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun tint(bar: ProgressBar, @ColorRes color: Int) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            val drawableProgress = DrawableCompat.wrap(bar.indeterminateDrawable)
            DrawableCompat.setTint(drawableProgress, ContextCompat.getColor(context!!, color))
            bar.indeterminateDrawable = DrawableCompat.unwrap(drawableProgress)

        } else {
            bar.indeterminateDrawable.setColorFilter(ContextCompat.getColor(context!!, color), PorterDuff.Mode.SRC_IN)
        }
    }

    override fun inject() {
        App.component.inject(this)
    }

    override fun onNewViewModel(viewModel: DetailViewModel) {
        collapsingToolbarLayout.title = viewModel.title
        title.text = viewModel.title
        viewModel.item?.let {
            Picasso.get()
                    .load("http://image.tmdb.org/t/p/w1280/" + it.backdropPath)
                    .placeholder(R.drawable.clock)
                    .fit()
                    .centerCrop()
                    .into(screenCapture)

            date.text = getString(R.string.year, formatDate(it.releaseDate))

            overview.text = it.overview
            totalVotes.text = getString(R.string.between_parens, it.voteCount.toString())
            rating.rating = it.voteAverage / 2  // rating is out of 10 points, and we have 5 stars
        }
    }

    private fun formatDate(dateStr: String) = when (dateStr.length) {
        in 0..4 -> dateStr
        else -> dateStr.substring(0, 4)
    }
}