package app.component.detail

import app.App
import app.model.ItemTypes
import app.model.ItemTypes.Companion.ItemType
import app.repo.EntertainmentRepository
import mvvm.MvvmPresenter
import javax.inject.Inject

class DetailPresenter(
        @ItemType val itemType: Int,
        val itemId: String,
        val title: String
) : MvvmPresenter<DetailViewModel>() {

    @Inject
    lateinit var repo: EntertainmentRepository

    init {
        App.component.inject(this)

        emitViewModel(DetailViewModel(null, title, true, null))

        addDisposable(
                repo.getItemDetail(itemId)
                        .map {
                            viewModel.copy(
                                    item = it.payload,
                                    error = it.error
                            )
                        }
                        .subscribe {
                            emitViewModel(it)
                        }
        )
    }
}
