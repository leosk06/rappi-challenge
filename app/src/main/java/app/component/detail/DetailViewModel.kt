package app.component.detail

import app.model.ScreenEntertainment

data class DetailViewModel(
        val item: ScreenEntertainment?,
        val title: String,
        val loading: Boolean,
        val error: Throwable?
)
