package app.component.main

import android.os.Bundle
import android.support.annotation.IdRes
import android.support.design.widget.BaseTransientBottomBar
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.App
import app.R
import app.component.category.CategoryFragment
import app.component.category.CategoryPresenter
import app.di.CategoryPresenters
import app.model.Categories
import app.model.Categories.Companion.Category
import app.model.ItemTypes
import app.model.ItemTypes.Companion.ItemType
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_main.*
import java.net.UnknownHostException


class MainFragment : Fragment() {
    private val categoryPresenters = ArrayList<CategoryPresenter>()

    private var snackbar: Snackbar? = null
    private var lastError: Throwable? = null

    private val disposables = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.component.inject(this)

        val activity = activity as AppCompatActivity
        activity.setSupportActionBar(toolbar)
        activity.supportActionBar!!.setDisplayShowHomeEnabled(true)

        setupCategory(R.id.popularMovieContainer, ItemTypes.MOVIE, Categories.POPULAR)
        setupCategory(R.id.topRatedMovieContainer, ItemTypes.MOVIE, Categories.TOP_RATED)
        setupCategory(R.id.upcomingMovieContainer, ItemTypes.MOVIE, Categories.UPCOMING)

        setupCategory(R.id.popularTvSeriesContainer, ItemTypes.TV_SHOW, Categories.POPULAR)
        setupCategory(R.id.topRatedTvSeriesContainer, ItemTypes.TV_SHOW, Categories.TOP_RATED)
    }

    private fun setupCategory(@IdRes containerId: Int, @ItemType itemType: Int, @Category category: Int) {
        categoryPresenters.add(CategoryPresenters.of(itemType, category))
        if (childFragmentManager.findFragmentById(containerId) == null) {
            val fragment = CategoryFragment.newInstance(itemType, category)
            childFragmentManager.beginTransaction()
                    .add(containerId, fragment)
                    .commit()
        }
    }

    override fun onStart() {
        super.onStart()
        categoryPresenters.forEach { presenter ->
            disposables.add(
                    presenter.observeViewStates
                            .filter { it.error != null }
                            .map { it.error!! }
                            .subscribe { error ->
                                Log.e("ERROR", "Error loading category", error)
                                showError(error)
                            }
            )
        }
    }

    override fun onStop() {
        super.onStop()
        snackbar?.let {
            it.dismiss()
            snackbar = null
            lastError = null
        }
        disposables.dispose()
    }

    private fun showError(error: Throwable) {
        if (error.message == lastError?.message) {
            return
        }
        lastError = error

        snackbar = Snackbar.make(
                coordinator,
                when (error) {
                    is UnknownHostException -> R.string.error_network_failure
                    else -> R.string.error_unknown
                },
                Snackbar.LENGTH_INDEFINITE
        )

        snackbar?.let {
            it.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                    super.onDismissed(transientBottomBar, event)
                    categoryPresenters.forEach { presenter ->
                        presenter.dismissedErrorMessage()
                    }
                }
            })
            it.show()
        }
    }
}