package app.component.category

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.App
import app.Navigator
import app.R
import app.di.CategoryPresenters
import app.model.Categories.Companion.Category
import app.model.ItemTypes.Companion.ItemType
import app.ui.ScreenEntertainmentAdapter
import kotlinx.android.synthetic.main.fragment_category.*
import mvvm.BaseFragment

open class CategoryFragment : BaseFragment<CategoryViewModel, CategoryPresenter>() {
    companion object {
        private const val ARG_ITEM_TYPE = "it"
        private const val ARG_CATEGORY = "cat"

        fun newInstance(@ItemType itemType: Int, @Category category: Int): CategoryFragment {
            val instance = CategoryFragment()
            val args = Bundle()
            args.putInt(ARG_ITEM_TYPE, itemType)
            args.putInt(ARG_CATEGORY, category)
            instance.arguments = args
            return instance
        }
    }

    lateinit var adapter: ScreenEntertainmentAdapter

    private var category: Int = -1
    private var itemType: Int = -1

    private var navigator: Navigator? = null


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        navigator = context as Navigator
    }

    override fun onDetach() {
        super.onDetach()
        navigator = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let { args ->
            category = args.getInt(ARG_CATEGORY)
            itemType = args.getInt(ARG_ITEM_TYPE)
        }
        presenter = CategoryPresenters.of(itemType, category)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // onViewCreated is called after onAttach, so getActivity() is non-null, hence the !!
        adapter = ScreenEntertainmentAdapter(activity!!, navigator!!)
        sectionList.adapter = adapter
        sectionList.        addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                    if (!recyclerView.canScrollHorizontally(1)) {
                        presenter.getNextPage()
                    }
            }


        })
    }

    override fun inject() {
        App.component.inject(this)
    }

    override fun onNewViewModel(viewModel: CategoryViewModel) {
        if (viewModel.error == null) {
            sectionTitle.text = viewModel.title
            adapter.setData(viewModel.items)
        }
    }
}
