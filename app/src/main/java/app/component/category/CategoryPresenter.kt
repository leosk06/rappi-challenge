package app.component.category

import app.App
import app.R
import app.model.Categories
import app.model.Categories.Companion.Category
import app.model.ItemTypes
import app.model.ItemTypes.Companion.ItemType
import app.model.ScreenEntertainment
import app.repo.EntertainmentRepository
import mvvm.MvvmPresenter
import java.util.*
import javax.inject.Inject

class CategoryPresenter(
        @ItemType val itemType: Int,
        @Category val category: Int
) : MvvmPresenter<CategoryViewModel>() {
    @Inject
    lateinit var repo: EntertainmentRepository

    var pageNumber = 1
    private val items = ArrayList<ScreenEntertainment>()

    init {
        App.component.inject(this)

        val title = App.instance.getString(
                when (itemType) {
                    ItemTypes.MOVIE -> when (category) {
                        Categories.POPULAR -> R.string.main_title_popular_movies
                        Categories.TOP_RATED -> R.string.main_title_top_rated_movies
                        else -> R.string.main_title_upcoming_movies
                    }
                    else -> when (category) {
                        Categories.POPULAR -> R.string.main_title_popular_tv_series
                        Categories.TOP_RATED -> R.string.main_title_top_rated_tv_series
                        else -> R.string.main_title_upcoming_tv_series
                    }
                }
        )
        emitViewModel(CategoryViewModel(
                itemType = itemType,
                title = title,
                items = Collections.emptyList(),
                error = null
        ))

        getNextPage()
    }

    override fun dismissedErrorMessage() {
        emitViewModel(viewModel.copy(error = null))
    }

    fun getNextPage() {
        addDisposable(
                when (itemType) {
                    ItemTypes.MOVIE -> repo.getMovieByCriteria(category, pageNumber)
                    else -> repo.getTvSeriesByCriteria(category, pageNumber)
                }.map {
                    val payload = it.payload!!

                    if (it.error == null && payload.isNotEmpty()) {
                        items.addAll(payload.filter { !items.contains(it) })
                        pageNumber += 1
                    }
                    viewModel.copy(items = Collections.unmodifiableList(items), error = it.error)
                }.subscribe {
                    emitViewModel(it)
                }
        )
    }
}
