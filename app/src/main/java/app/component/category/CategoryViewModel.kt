package app.component.category

import app.model.ItemTypes.Companion.ItemType
import app.model.ScreenEntertainment
import java.util.*

data class CategoryViewModel(
        @ItemType
        val itemType: Int,
        val title: String,
        val items: List<ScreenEntertainment> = Collections.emptyList(),
        val error: Throwable?
)
