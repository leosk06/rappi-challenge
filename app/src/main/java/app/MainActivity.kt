package app

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import app.component.detail.DetailFragment
import app.component.main.MainFragment
import app.model.ScreenEntertainment
import app.repo.EntertainmentRepository
import app.ui.search.OnSuggestionSelectedListener
import app.ui.search.SuggestionsAdapter
import io.reactivex.disposables.CompositeDisposable
import java.lang.*
import java.lang.reflect.InvocationTargetException
import javax.inject.Inject

class MainActivity : AppCompatActivity(), Navigator, OnSuggestionSelectedListener {
    @Inject
    lateinit var repo: EntertainmentRepository

    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        App.component.inject(this)

        if (supportFragmentManager.findFragmentById(R.id.root) == null) {
            goto(MainFragment(), false)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_main, menu)

        // at onCreateOptionsMenu() we should be attached to an Activity, hence the !!
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        val searchItem = menu.findItem(R.id.search)
        val searchView = searchItem.actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setIconifiedByDefault(false)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?) = true

            override fun onQueryTextChange(query: String): Boolean {
                if (query.length > 1) {
                    disposable.add(
                            repo.search(query).subscribe(
                                    { results ->
                                        if (results.isNotEmpty()) {
                                            val adapter = SuggestionsAdapter(this@MainActivity, results)
                                            adapter.itemClickListener = this@MainActivity
                                            searchView.suggestionsAdapter = adapter
                                        }
                                    },
                                    { Log.e("", "Error during search", it) }
                            )
                    )
                }
                return false
            }
        })

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()

        /*
            Attempt to prevent Activity leak through InputMethodManager.

            LeakCanary may say it's Android's fault, but leak is still there, and that bothers
            especially on lower end phones.

            And reflection may be "wrong", but I think it's justifiable here, because it's only done
            at onDestroy(), that is not really often, and besides it's guarded by a try..catch

         */
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager? ?: return
        try {
            imm::class.java.declaredFields
                    .filter {
                        if (!it.isAccessible) {
                            it.isAccessible = true
                        }
                        it.isAccessible
                    }
                    .filter { accessibleField ->
                        val value = accessibleField.get(imm)
                        value != null && value is View
                    }
                    .forEach { accessibleViewField ->
                        accessibleViewField.set(imm, null)
                    }
        } catch (e: Exception) {
            if (
                    e is IllegalAccessException
                    || e is InstantiationException
                    || e is InvocationTargetException
                    || e is NoSuchFieldException
                    || e is NoSuchMethodException) {
                // Ignore invalid field access. Leak is inevitable
            }
            throw e
        }


    }

    private fun goto(f: Fragment, addToBackstack: Boolean) {
        val transaction = supportFragmentManager.beginTransaction()
        if (addToBackstack) {
            transaction.setCustomAnimations(
                    R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right)
        }
        transaction.replace(R.id.root, f, f.javaClass.simpleName)
        if (addToBackstack) {
            transaction.addToBackStack(null)
        }
        transaction.commit()
    }

    override fun gotoDetail(item: ScreenEntertainment) {
        goto(DetailFragment.newInstance(item), true)
    }

    override fun onSuggestionSelected(selected: ScreenEntertainment) {
        gotoDetail(selected)
    }
}
