package app.http.model

import com.google.gson.annotations.SerializedName

class ProductionCompany {
    var id: String? = null
    var name: String? = null

    @SerializedName("logo_path")
    var logoPath: String? = null

    @SerializedName("origin_country")
    var country: String? = null
}
