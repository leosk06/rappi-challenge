package app.http.model

import com.google.gson.annotations.SerializedName

class Language {
    @SerializedName("iso_639_1")
    var id: String? = null

    var name: String? = null
}
