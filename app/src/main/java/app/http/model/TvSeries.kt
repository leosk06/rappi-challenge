package app.http.model

import com.google.gson.annotations.SerializedName

class TvSeries : ScreenEntertainment() {
    @SerializedName("origin_country")
    var countries: Array<String>? = null

    @SerializedName("original_name")
    var originalName: String? = null
}