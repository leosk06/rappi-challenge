package app.http.model

import com.google.gson.annotations.SerializedName

class Country {
    @SerializedName("iso_3166_1")
    var id: String? = null

    var name: String? = null
}
