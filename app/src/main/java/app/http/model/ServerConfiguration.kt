package app.http.model

import com.google.gson.annotations.SerializedName

class ServerConfiguration {
    var images: Images? = null

    class Images {
        @SerializedName("base_url")
        var baseUrl: String? = null

        @SerializedName("secure_base_url")
        var secureBaseUrl: String? = null

        @SerializedName("backdrop_sizes")
        var backdropSizes: Array<String>? = null

        @SerializedName("logo_sizes")
        var logoSizes: Array<String>? = null

        @SerializedName("poster_sizes")
        var posterSizes: Array<String>? = null

        @SerializedName("profile_sizes")
        var profileSizes: Array<String>? = null

        @SerializedName("still_sizes")
        var stillSizes: Array<String>? = null
    }
}