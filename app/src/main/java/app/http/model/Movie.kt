package app.http.model

import com.google.gson.annotations.SerializedName

class Movie: ScreenEntertainment() {
    var adult = false

    var homepage: String? = null

    @SerializedName("imdb_id")
    var imdbId: String? = null

    @SerializedName("original_title")
    var originalTitle: String? = null

    @SerializedName("genres")
    var genres: Array<Genre>? = null

    var budget = -1
    var revenue = -1

    @SerializedName("runtime")
    var runtimeMins = -1

    @SerializedName("production_companies")
    var productionCompanies: Array<ProductionCompany>? = null

    @SerializedName("production_countries")
    var productionCountries: Array<Country>? = null

    @SerializedName("spoken_languages")
    var spokenLanguages: Array<Language>? = null

    var status: String? = null

    @SerializedName("tag_line")
    var tagLine: String? = null
}
