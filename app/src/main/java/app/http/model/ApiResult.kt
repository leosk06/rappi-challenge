package app.http.model

import com.google.gson.annotations.SerializedName

class ApiResult<T> {
    lateinit var results: Array<T>

    var page = 0

    @SerializedName("total_pages")
    var totalPages = 0

    @SerializedName("total_results")
    var totalResults = 0

    lateinit var dates: ResultDates

    class ResultDates {
        lateinit var maximum: String
        lateinit var minimum: String
    }
}