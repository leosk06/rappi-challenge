package app.http.model

import com.google.gson.annotations.SerializedName

abstract class ScreenEntertainment {
    var id: String? = null

    @SerializedName("vote_count")
    var voteCount = -1

    @SerializedName(value = "title", alternate = ["name"])
    var title: String? = null


    @SerializedName("vote_average")
    var voteAverage = -1.0

    var popularity = -1.0

    @SerializedName("poster_path")
    var posterPath: String? = null

    @SerializedName("original_language")
    var originalLanguage: String? = null

    @SerializedName("genre_ids")
    var genreIds: Array<Int>? = null

    @SerializedName("backdrop_path")
    var backdropPath: String? = null

    var overview: String? = null

    @SerializedName(value ="release_date", alternate = ["first_air_date"])
    var releaseDate: String? = null

    @SerializedName("video")
    var hasVideo = false
}
