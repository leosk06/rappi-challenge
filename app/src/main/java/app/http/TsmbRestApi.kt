package app.http

import android.support.annotation.StringDef
import app.http.model.ApiResult
import app.http.model.Movie
import app.http.model.ServerConfiguration
import app.http.model.TvSeries
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TsmbRestApi {
    companion object {
        const val CRITERIA_POPULAR = "popular"
        const val CRITERIA_UPCOMING = "upcoming"
        const val CRITERIA_TOP_RATED = "top_rated"
    }

    @StringDef(CRITERIA_POPULAR, CRITERIA_TOP_RATED, CRITERIA_UPCOMING)
    annotation class Criteria


    @GET("configuration")
    fun getConfiguration(): Observable<ServerConfiguration>

    @GET("movie/{criteria}")
    fun getMovieByCriteria(@Criteria @Path("criteria") criteria: String, @Query("page") page: Int): Observable<ApiResult<Movie>>

    @GET("tv/{criteria}")
    fun getTvSeriesByCriteria(@Criteria @Path("criteria") criteria: String, @Query("page") page: Int): Observable<ApiResult<TvSeries>>

    @GET("movie/{id}")
    fun getMovieDetail(@Path("id") id: String): Observable<Movie>

    @GET("tv/{id}")
    fun getTvSeriesDetail(@Path("id") id: String): Observable<TvSeries>

    @GET("search/movie")
    fun searchMovies(@Query("language") language: String, @Query("query") query: String, @Query("page") page: Int, @Query("include_adult") includeAdult: Boolean): Observable<ApiResult<Movie>>

    @GET("search/tv")
    fun searchTvShows(@Query("language") language: String, @Query("query") query: String, @Query("page") page: Int): Observable <ApiResult<TvSeries>>
}
